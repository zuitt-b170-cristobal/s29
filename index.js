//this require("express") allows devs to load/import express package that will be usewd for the app
let express = require("express");

//express() - allows devs to create an app using express
const app = express();//this code creates an express app and stores it inside the "app" variable. thus, "app" is the server already.

const port = 3000;

//use function lets the middleware to do common services & capabilities to the apps; MUST HAVE at all times
  //not using these 2 will result to our req.body to return undefined

app.use(express.json());//lets app read json data
app.use(express.urlencoded({extended: true})); //allows app to receive data from forms
/*
//Express has methods corresponding to each http methods (get, post, put, del, etc)
//this "/" route expects to receive a get request at its endpoint (http://localhost3000/)
app.get("/", (req, res) => {
	//res.send - allows sending of messages as responses to the client
	res.send("Hello World")
})
*/


//Mock Database
let database = [
{
	"userName": "Brandon",
	"password": "maxwell098"
},
{
	"userName": "Tory",
	"password": "burch123"
}
]

//MINIACTIVITY
app.get("/hello", (req, res) => {
	res.send("Hello from the /hello endpoint")
})


//this "/hello" route is expected to receive a post request that has json data in the body
app.post("/hello", (req, res) => {
	//to check if the json keys in the body has values
	//console.log(req.body.firstName);
	//console.log(req.body.lastName);
	//sends response once the req.body.firstName & req.body.lastName has values
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
})

//1 GET route that will access the "/home" route that will print out a simple message
app.get("/home", (req, res) => {
	res.send("Welcome home!")
})

//Create a GET route that will access the "/users" route that will retrieve all the users in the mock database
app.get("/users", (req, res) => {
	res.send(database)
})

//Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
app.delete("/delete-user", (req, res) => {
	res.send(`User ${req.body.userName} has been deleted`)
})

//Stretch
app.post("/signup", (req, res, next) => {
	if (req.userName != "" && req.password != "") {
	let container = ""
	req.on("data", function(data){
			container += data
		})
		req.on("end", function() {
			container = JSON.parse(container)

			let newUser = {
				"userName": container.userName,
				"password": container.password
			}
			database.push(newUser);
			res.send(`User ${req.body.userName} successfully registered`)
		}) 
	}  
 	else next() 
 }, (req, res, next) => {
  	res.send("Please input both username and password")
})


app.listen(port, () => console.log(`Server is running at port ${port}`)); //must be only 1




